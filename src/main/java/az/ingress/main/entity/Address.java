package az.ingress.main.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "ADDRESS")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Address {

    @Id
    @GeneratedValue(generator = "ADDRESS")
    Long id;

    @Column(nullable = false)
    String location;

    @OneToOne(mappedBy = "address")
    Library library;

}
