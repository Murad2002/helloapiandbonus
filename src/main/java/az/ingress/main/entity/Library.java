package az.ingress.main.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "LIBRARY")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Library {

    @Id
    @GeneratedValue(generator = "LIBRARY")
    Long id;

    @Column(nullable = false)
    String name;

    @OneToOne
    @JoinColumn(name = "address_id")
    Address address;
}
