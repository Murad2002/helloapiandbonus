package az.ingress.main.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "STUDENT")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    Long id;

    @Column(name = "NAME")
    String name;

    @Column(name = "SURNAME")
    String surname;

    @Column(name = "DOB")
    LocalDate dob;

    @Column(name = "AGE")
    Integer age;

    @Column(name = "HEIGHT")
    Double height;

    @Column(name = "INSTITUTE")
    String institute;

    @Column(name = "ACTIVE")
    @ColumnDefault(value = "1")
    Integer status;

    @Column(name = "INSERT_DATE")
    @CreationTimestamp
    LocalDateTime insertDate;

}


