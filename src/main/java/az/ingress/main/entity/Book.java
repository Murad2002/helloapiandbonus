package az.ingress.main.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "BOOK")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Book {
    @Id
    @GeneratedValue(generator = "BOOK")
    Long id;

    @Column(nullable = false)
    String title;

    @ManyToOne
    @JoinColumn(name = "library_id")
    Library library;
}
