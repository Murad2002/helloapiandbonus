package az.ingress.main.service.impl;


import az.ingress.main.dto.StudentDto;
import az.ingress.main.entity.Student;
import az.ingress.main.repository.StudentRepository;
import az.ingress.main.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {


    private final StudentRepository studentRepository;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    @Override
    public ResponseEntity<HttpStatus> signUp(StudentDto studentDto) {

        ResponseEntity<HttpStatus> response = null;
        if (studentDto == null || studentDto.getName() == null || studentDto.getSurName() == null) {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else{
            studentRepository.save(convertToStudent(studentDto));
            response = new ResponseEntity<>(HttpStatus.OK);
        }

        return response;
    }

    @Override
    public ResponseEntity<List<StudentDto>> getStudentList() {
        ResponseEntity<List<StudentDto>> response = null;
        List<StudentDto> studentDtoList = studentRepository.findAllByStatus(1)
                .stream()
                .map(this::convertToStudentDto)
                .collect(Collectors.toList());
        response = new ResponseEntity<>(studentDtoList,HttpStatus.OK);
        return response;
    }

    @Override
    public ResponseEntity<HttpStatus> updateStudent(StudentDto studentDto) {
        ResponseEntity<HttpStatus> response = null;
        Student student = studentRepository.findById(studentDto.getId()).orElse(null);
        if(student == null){
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else{
            if(studentDto.getName() !=null && studentDto.getName() != student.getName()){
                student.setName(studentDto.getName());
            }
            if(studentDto.getSurName() !=null && studentDto.getSurName() != student.getSurname()){
                student.setSurname(studentDto.getSurName());
            }
            if(studentDto.getInstitute() !=null && studentDto.getInstitute() != student.getInstitute()){
                student.setInstitute(studentDto.getInstitute());
            }
            if(studentDto.getDob() !=null && studentDto.getDob() != student.getDob().toString()){
                student.setDob(LocalDate.parse(studentDto.getDob(),formatter));
            }
            studentRepository.save(student);
            response = new ResponseEntity<>(HttpStatus.OK);
        }
        return response;
    }

    @Override
    public ResponseEntity<HttpStatus> deleteStudent(Long id) {
        ResponseEntity<HttpStatus> response = null;
        Student student = studentRepository.findById(id).orElse(null);
        if(student == null){
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else{
            studentRepository.delete(student);
            response = new ResponseEntity<>(HttpStatus.OK);
        }
        return response;
    }


    public Student convertToStudent(StudentDto studentDto) {
        return Student.builder()
                .name(studentDto.getName())
                .surname(studentDto.getSurName())
                .dob(LocalDate.parse(studentDto.getDob(),formatter))
                .age(studentDto.getAge())
                .height(studentDto.getHeight())
                .institute(studentDto.getInstitute())
                .build();
    }

    public StudentDto convertToStudentDto(Student student) {
        return StudentDto.builder()
                .name(student.getName())
                .surName(student.getSurname())
                .dob(student.getDob().toString())
                .age(student.getAge())
                .height(student.getHeight())
                .institute(student.getInstitute())
                .build();
    }
}
