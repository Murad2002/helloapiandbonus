package az.ingress.main.service;

import az.ingress.main.dto.StudentDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {
    ResponseEntity<HttpStatus> signUp(StudentDto studentDto);

    ResponseEntity<List<StudentDto>> getStudentList();

    ResponseEntity<HttpStatus> updateStudent(StudentDto studentDto);

    ResponseEntity<HttpStatus> deleteStudent(Long id);
}
