package az.ingress.main.controller;

import az.ingress.main.dto.StudentDto;
import az.ingress.main.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/")
    public ResponseEntity<List<StudentDto>> getStudentList() {
        log.info("endpoint called: / , method: GET");
        return studentService.getStudentList();
    }

    @PostMapping("/signUp")
    public ResponseEntity<HttpStatus> insertStudent(@RequestBody StudentDto studentDto) {
        log.info("endpoint called: /signUp , method: POST");
        return studentService.signUp(studentDto);
    }


    @PutMapping("/update")
    public ResponseEntity<HttpStatus> updateStudent(@RequestBody StudentDto studentDto) {
        log.info("endpoint called: /update , method: PUT");
        return studentService.updateStudent(studentDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteStudent(@PathVariable Long id) {
        log.info("endpoint called: /delete , method: DELETE");
        return studentService.deleteStudent(id);
    }
}
