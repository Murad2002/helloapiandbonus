package az.ingress.main.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/main")
@Slf4j
public class MainController {

    @GetMapping("/sayHello/{name}")
    public String sayHello(@PathVariable String name){
        log.info("endpoint called: /sayHello/{} , method: GET",name);
        return "Hello , "+ name;
    }
}
